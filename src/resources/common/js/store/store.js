import Vue from 'vue';
import Vuex from 'vuex';
import {auth} from "@/js/arm/vue/store/modules/auth";
import {sidebar} from "@/js/arm/vue/store/modules/sidebar";
import {messages} from "@/js/arm/vue/store/modules/messages";
import {auth as authApi, dashboard} from "@/js/arm/api";

Vue.use(Vuex);

export const store = new Vuex.Store({
    strict: process.env.NODE_ENV !== 'production',
    state: {
        ready: false,
    },
    modules: {
       auth, sidebar, messages, 
    },
    mutations: {
        setReady(state, val) {
            state.ready = val;
        }
    },
    actions: {
        async fetchDashboard(context) {
            if (store.state.auth.token) {
                const res = await dashboard.fetch();
                context.commit('auth/setUser', res.user);
            }
        },
    }
});


