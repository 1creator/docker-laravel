import moment from "moment";

const state = {
    items: []
};

const getters = {};

const actions = {
    add(context, {message, options = {}}) {
        context.commit('add', {
            message: message
        });

        if (options.duration) {
            setTimeout(function () {
                context.commit('remove', {message: message});
            }, options.duration);
        }
    }
};

const mutations = {
    add(state, message) {
        message.id = Math.random().toString(36).substring(7);
        message.createdAt = moment();
        state.items.push(message);
    },
    remove(state, id) {
        let index = state.items.findIndex(message => message.id === id);
        if (index >= 0) {
            state.items.splice(index, 1);
        }
    }
};

export const messages = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
