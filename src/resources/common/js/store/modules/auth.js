import {auth as authApi} from "@/js/arm/api";
import {router} from "@/js/arm/vue/router";

const localStorageTokenKey = 'arm_access_token';
const token = localStorage.getItem(localStorageTokenKey);

const state = {
    token: token,
    user: null,
    permissions: [],
};

const mutations = {
    setUser(state, user) {
        if (user.permissions) {
            state.permissions = user.permissions;
            delete user.permissions;
        }
        state.user = user;
    },
    setToken(state, token) {
        state.token = token;

        if (token) {
            localStorage.setItem(localStorageTokenKey, token);
        } else {
            localStorage.removeItem(localStorageTokenKey);
        }
    },
};

const actions = {
    async login(context, {username, password}) {
        const res = await authApi.login(username, password);
        context.commit('setToken', res.accessToken);
    },
    async logout(context) {
        context.commit('setToken', null);
        context.commit('setReady', true, {root: true});
        router.push({name: 'auth.login'})
    },
};

const getters = {};

export const auth = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
};
