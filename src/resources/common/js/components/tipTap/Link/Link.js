import {Mark} from 'tiptap'
import {pasteRule, removeMark, updateMark} from 'tiptap-commands'

export default class Link extends Mark {

    get name() {
        return 'link'
    }

    get defaultOptions() {
        return {
            openOnClick: true,
        }
    }

    get schema() {
        return {
            attrs: {
                href: {
                    default: null,
                },
                rel: {
                    default: null,
                },
            },
            inclusive: false,
            parseDOM: [
                {
                    tag: 'a[href]',
                    getAttrs: dom => ({
                        href: dom.getAttribute('href'),
                    }),
                },
            ],
            toDOM: node => ['a', node.attrs, 0],
        }
    }

    commands({type, state}) {
        return attrs => {
            if (attrs.href) {
                return updateMark(type, attrs)
            }

            return removeMark(type)
        }
    }

    pasteRules({type}) {
        return [
            pasteRule(
                /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-zA-Z]{2,}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/g,
                type,
                url => ({href: url}),
            ),
        ]
    }
}
