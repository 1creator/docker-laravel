import {Node} from 'tiptap'

export default class Iframe extends Node {

    get name() {
        return 'iframe'
    }

    get schema() {
        return {
            attrs: {
                html: {
                    default: null,
                },
            },
            group: 'block',
            selectable: true,
            parseDOM: [{
                tag: 'iframe',
                getAttrs: dom => ({
                    html: dom.outerHTML,
                }),
            }],
            toDOM: node => {
                const tmpNode = document.createElement('div');
                tmpNode.innerHTML = node.attrs.html;
                if (tmpNode.firstChild.tagName != 'IFRAME') {
                    return;
                }

                return [
                    'iframe', {
                        src: tmpNode.firstChild.src,
                    }
                ];
            },
        }
    }

    commands({type}) {
        return attrs => (state, dispatch) => {
            const {selection} = state
            const position = selection.$cursor ? selection.$cursor.pos : selection.$to.pos
            const node = type.create(attrs)
            const transaction = state.tr.insert(position, node)
            dispatch(transaction)
        }
    }

    get view() {
        return {
            props: ['node', 'updateAttrs', 'view'],
            computed: {
                html: {
                    get() {
                        return this.node.attrs.html
                    },
                    set(html) {
                        this.updateAttrs({
                            html,
                        })
                    },
                },
            },
            template: `
        <div class="embed" v-html="html"></div>
      `,
        }
    }
}
