export default {
    inserted: function (el) {
        el.addEventListener('input', function () {
            this.classList.toggle('filled', this.value);
        });
        el.classList.toggle('filled', el.value);

    },
    componentUpdated: function (el) {
        el.classList.toggle('filled', el.value)
    }
};
