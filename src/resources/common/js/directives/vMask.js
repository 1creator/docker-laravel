import IMask from "imask/dist/imask.min";

export default {
    inserted: function (el, binding, vnode) {
        const maskOptions = {lazy: false,};
        if (binding.modifiers.phone) {
            maskOptions.mask = '+{7}(000)000-00-00';
            maskOptions.overwrite = true;
        }
        if (binding.modifiers.lazy) {
            maskOptions.lazy = true;
        }
        if (!maskOptions.lazy)
            el.classList.toggle('filled', true);

        el.IMask = new IMask(el, maskOptions);

        el.addEventListener('input', function (event) {
            if (event._maskEvent) return;
            triggerInput(el);
        });
        triggerInput(el);
    },
    update(el, binding, vnode) {
        // el.IMask.updateControl();
    }
}

function triggerInput(el) {
    const eventForDataUpdate = new Event('input', {'bubbles': false});
    eventForDataUpdate._maskEvent = true;
    el.dispatchEvent(eventForDataUpdate);
}
