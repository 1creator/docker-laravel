<?php


namespace App\Services\Stats\Interfaces;


interface StatsInterface
{
    const ACTIVITY_VIEW = 'activity:view';
    const ACTIVITY_CONTACTS = 'activity:contacts';
    const ACTIVITY_MAP = 'activity:map';
    const ACTIVITY_VK = 'activity:vk';
    const ACTIVITY_OK = 'activity:ok';
    const ACTIVITY_INST = 'activity:inst';
    const ACTIVITY_FB = 'activity:fb';

    const SCHOOL_VIEW = 'school:view';
    const SCHOOL_CONTACTS = 'school:contacts';
    const SCHOOL_MAP = 'school:map';
    const SCHOOL_VK = 'school:vk';
    const SCHOOL_OK = 'school:ok';
    const SCHOOL_INST = 'school:inst';
    const SCHOOL_FB = 'school:fb';

    public function hit($model, string $type);

    public function getStats($model, string $type, $from = null, $to = null);

    public function total($model, string $type, $from = null, $to = null);

    public function getTopStats($model, string $type, string $field, $from = null, $to = null, int $count = 10);

}
