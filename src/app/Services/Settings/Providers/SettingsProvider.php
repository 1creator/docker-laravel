<?php

namespace App\Services\Settings\Providers;

use App\Services\Payment\Controllers\PaymentController;
use App\Services\Payment\Interfaces\PaymentInterface;
use App\Services\Payment\Services\TransactionService;
use App\Services\Payment\Services\YandexKassaService;
use App\Services\Settings\Models\Setting;
use App\Services\Settings\Services\SettingsService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class SettingsProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SettingsService::class, SettingsService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Migrations');
    }
}
