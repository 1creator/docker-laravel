<?php


namespace App\Services\Payment\Interfaces;


use Illuminate\Http\Request;
use Illuminate\Http\Response;

interface PaymentInterface
{
    public function makeIframe(int $amount, string $description): string;

    public function makeRedirect(int $amount, string $description, string $return_url): string;

    public function resolveWebHook(Request $request): Response;
}
